import { createGlobalStyle } from 'styled-components';

import LimelightRegular from './Limelight-Regular.woff';
import SofiaProRegular from './SofiaProRegular.woff';
import SofiaProMedium from './SofiaProMedium.woff2';
import SofiaProLight from './SofiaProLight.woff';

export default createGlobalStyle`
    @font-face {
        font-family: 'Limelight';
        src: local('Limelight'),
        url(${LimelightRegular}) format('woff');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
      font-family: 'Sofia Pro Light';
      src: local('Sofia Pro Light'), local('SofiaProLight'),
      url(${SofiaProLight}) format('woff');
    }

    @font-face {
      font-family: 'Sofia Pro Regular';
      src: local('Sofia Pro Regular'), local('SofiaProRegular'),
      url(${SofiaProRegular}) format('woff');
      font-weight: 300;
      font-style: normal;
    }

    @font-face {
      font-family: 'Sofia Pro Medium';
      src: local('Sofia Pro Medium'), local('SofiaProMedium'),
      url(${SofiaProMedium}) format('woff2');
    }
`;
