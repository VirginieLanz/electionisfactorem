import Homepage from "./pages/Homepage";
import styled, {ThemeProvider} from 'styled-components';
import GlobalFonts from './fonts/fonts';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import Chapter1 from "./pages/Chapter1";
import Chapter2 from "./pages/Chapter2";
import Chapter3 from "./pages/Chapter3";
import Level1 from "./components/Levels/Level1/Level1";
import Level2 from "./components/Levels/Level2/Level2";
import Level1Choice1 from "./components/Levels/Level1/Choices/Choice1";
import Level1Choice2 from "./components/Levels/Level1/Choices/Choice2";
import Level1Choice3 from "./components/Levels/Level1/Choices/Choice3";
import Level2Choice1 from "./components/Levels/Level2/Choices/Choice1";
import Level2Choice2 from "./components/Levels/Level2/Choices/Choice2";
import Level2Choice3 from "./components/Levels/Level2/Choices/Choice3";
import BoarGamePage from "./pages/Boar/BoarGamePage";
import BoarGame from "./components/Levels/Level1/Game/BoarGame";
import FeatherGame from "./components/Levels/Level1/Game/FeatherGame";
import BoarGameOver from "./pages/Boar/BoarGameOver";
import FeatherGamePage from "./pages/Feather/FeatherGamePage";
import FeatherGameOver from "./pages/Feather/FeatherGameOver";
import AsteroidsGame from "./components/Levels/Level1/Game/AsteroidsGame";
import AsteroidsGameOver from "./pages/Asteroids/AsteroidsGameOver";
import AsteroidsGamePage from "./pages/Asteroids/AsteroidsGamePage";
import AsteroidsGameWon from "./pages/Asteroids/AsteroidsGameWon";
import FeatherGameWon from "./pages/Feather/FeatherGameWon";
import BoarGameWon from "./pages/Boar/BoarGameWon";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <GlobalFonts />
            <GameJam>
                <Router>
                    <Switch>
                        <Route exact path="/">
                            <Homepage/>
                        </Route>
                        <Route exact path="/homepage">
                            <Homepage/>
                        </Route>
                        <Route exact path="/chapter-1">
                            <Chapter1/>
                        </Route>
                        <Route exact path="/chapter-2">
                            <Chapter2/>
                        </Route>
                        <Route exact path="/chapter-3">
                            <Chapter3/>
                        </Route>
                        <Route exact path="/level-1">
                            <Level1/>
                        </Route>
                        <Route exact path="/level-2">
                            <Level2/>
                        </Route>
                        <Route exact path="/level-1-choice-1">
                            <Level1Choice1/>
                        </Route>
                        <Route exact path="/level-1-choice-2">
                            <Level1Choice2/>
                        </Route>
                        <Route exact path="/level-1-choice-3">
                            <Level1Choice3/>
                        </Route>
                        <Route exact path="/level-2-choice-1">
                            <Level2Choice1/>
                        </Route>
                        <Route exact path="/level-2-choice-2">
                            <Level2Choice2/>
                        </Route>
                        <Route exact path="/level-2-choice-3">
                            <Level2Choice3/>
                        </Route>
                        <Route exact path="/mini-boargame">
                            <BoarGamePage/>
                        </Route>
                        <Route exact path="/boargame-play">
                            <BoarGame/>
                        </Route>
                        <Route exact path="/boargame-over">
                            <BoarGameOver/>
                        </Route>
                        <Route exact path="/mini-feathergame">
                            <FeatherGamePage/>
                        </Route>
                        <Route exact path="/feathergame-play">
                            <FeatherGame/>
                        </Route>
                        <Route exact path="/feathergame-over">
                            <FeatherGameOver/>
                        </Route>
                        <Route exact path="/mini-asteroidsgame">
                            <AsteroidsGamePage/>
                        </Route>
                        <Route exact path="/asteroidsgame-play">
                            <AsteroidsGame/>
                        </Route>
                        <Route exact path="/asteroidsgame-over">
                            <AsteroidsGameOver/>
                        </Route>
                        <Route exact path="/boargame-won">
                            <BoarGameWon/>
                        </Route>
                        <Route exact path="/asteroidsgame-won">
                            <AsteroidsGameWon/>
                        </Route>
                        <Route exact path="/feathergame-won">
                            <FeatherGameWon/>
                        </Route>
                    </Switch>
                </Router>
            </GameJam>
        </ThemeProvider>
    );
}

const GameJam = styled.div`
  font-family: ${props => props.theme.sofiaProRegular};
`
const theme = {
    primary: 'red',
    secondary: 'blue',
    white: 'white',
    transparentWhite: 'rgba(255, 255, 255, 0.4)',
    transparentWhite90: 'rgba(255, 255, 255, 0.1)',
    darkGrey: '#362C36',
    mediumGrey: '#B7B6B6',
    lightGrey: '#E0E0E0',
    extraSmall: '0.8rem',
    small: '1rem',
    medium: '1.2rem',
    large: '1.5rem',
    extraLarge: '2.5rem',
    limeLight : 'Limelight',
    sofiaProLight : 'Sofia Pro Light',
    sofiaProRegular : 'Sofia Pro Regular',
    sofiaProMedium : 'Sofia Pro Medium'
};

export default App;
