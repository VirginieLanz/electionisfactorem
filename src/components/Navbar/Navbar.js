import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/logo_EF.svg';

function Navbar() {
    return (
        <Nav>
            <img src={logo} alt="Logo d'Electionis Factorem"/>
        </Nav>
    );
}

const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  top: 20px;
  height: 50px;
  z-index: 1;
  img {
    height: 200px;
  }
`;

export default Navbar;
