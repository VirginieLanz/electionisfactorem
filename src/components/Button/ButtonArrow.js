import React from 'react';
import styled from 'styled-components';

function ButtonArrow({onClick, imageUrl}) {
    return (
        <Content onClick={onClick}>
            <img src={imageUrl} alt=""/>
        </Content>
    );
}

const Content = styled.button`
  position: relative;
  z-index: 99;
  border: none;
  background: none;
  cursor: pointer;
  img {
    width: 20px;
  }
`;

export default ButtonArrow;
