import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

function Button(props) {
    return (
        <Content as={Link} to={props.link} className={props.class}>
            {props.text}
        </Content>
    );
}

const Content = styled.button`
  padding: 0.7rem 1.8rem;
  border: 2px solid white;
  box-shadow: 0 0 20px 1px rgba(255, 255, 255, 0.5);
  border-radius: 50px;
  background: ${props => props.theme.white};
  cursor: pointer;
  color: ${props => props.theme.darkGrey};
  font-family: ${props => props.theme.sofiaProRegular};
  font-size: ${props => props.theme.small};
  text-decoration: none;
  
  &.primaryButton {
    font-size: ${props => props.theme.extraSmall};
    padding: 8px 20px;
    border: none;
    box-shadow: initial;
  }
  &.secondaryButton {
    color: ${props => props.theme.white};
    background: ${props => props.theme.transparentWhite};
    font-size: ${props => props.theme.extraSmall};
    padding: 8px 20px;
    border: none;
    box-shadow: initial;
  }

  &.thirdButton {
    color: ${props => props.theme.white};
    border: 1px solid ${props => props.theme.white};
    background: ${props => props.theme.transparentWhite};
    font-size: ${props => props.theme.extraSmall};
    padding: 8px 20px;
    border: none;
    box-shadow: initial;
  }
  
  &.disableButton {
    color: ${props => props.theme.mediumGrey};
    background: ${props => props.theme.lightGrey};
    font-size: ${props => props.theme.extraSmall};
    padding: 8px 20px;
    border: none;
    box-shadow: initial;
  }
`;

export default Button;
