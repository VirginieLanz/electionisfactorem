import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

function ButtonRounded(props) {
    return (
        <Content as={Link} to={props.link}>
            <img src={props.content} alt=""/>
        </Content>
    );
}

const Content = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60px;
  width: 60px;
  border-radius: 50%;
  background: ${props => props.theme.white};
  
  img {
    width: 1.5em;
  }
`;

export default ButtonRounded;
