import React from 'react';
import styled from 'styled-components';

function DialogueComponent({text, buttonContent}) {
    return (
        <Content className="dialogue">
            <Paragraph>{text}</Paragraph>
            <div>{buttonContent}</div>
        </Content>
    );
}

const Content = styled.div`
  min-width: 60%;
  max-width: 60%;
  border-radius: 20px;
  padding: 20px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
  background: ${props => props.theme.transparentWhite};
  color: ${props => props.theme.white};
  border: 1px solid ${props => props.theme.white};
  -webkit-box-shadow: 0 0 6px 0 ${props => props.theme.white};
  box-shadow: 0 0 6px 0 ${props => props.theme.white};
`;

const Paragraph = styled.div`
  margin-bottom: 50px;
  white-space: pre-line;
`


export default DialogueComponent;
