import React from 'react';
import styled from 'styled-components';
import Button from "../Button/Button";

function ParagraphComponent(props) {
    return (
        <Content className="paragraph-component">
            <Title>{props.title}</Title>
            <Paragraph>{props.content}</Paragraph>
            <img src={props.image} alt=""/>
            <Paragraph>{props.secondContent}</Paragraph>
            <Button text={props.text} class="secondaryButton" link={props.link}/>
        </Content>
    );
}

const Content = styled.div`
  width: 60%;
  border-radius: 20px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
  background: ${props => props.theme.transparentWhite};
  color: ${props => props.theme.white};
  border: 1px solid ${props => props.theme.white};
  -webkit-box-shadow: 0 0 6px 0 ${props => props.theme.white};
  box-shadow: 0 0 6px 0 ${props => props.theme.white};
  white-space: pre-line;
  img {
    width: 40%;
    margin: 20px 0;
  }
`;

const Title = styled.p`
  font-family: ${props => props.theme.limeLight};
  font-size: ${props => props.theme.extraLarge};;
`

const Paragraph = styled.p`
    margin: 20px 0;
  font-family: ${props => props.theme.sofiaProLight};
`


export default ParagraphComponent;
