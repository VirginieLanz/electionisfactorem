import React from 'react';
import styled from 'styled-components';

function NarrationComponent({text, buttonContent}) {
    return (
        <Content className="narration">
            <Paragraph className="narration__text">{text}</Paragraph>
            <div className="narration__button">{buttonContent}</div>
        </Content>
    );
}

const Content = styled.div`
  min-width: 80%;
  max-width: 80%;
  padding: 20px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
`;

const Paragraph = styled.div`
  margin-bottom: 50px;
  white-space: pre-line;
`


export default NarrationComponent;
