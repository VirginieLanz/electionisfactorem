import React, {useState, useEffect} from 'react';
import {Redirect} from "react-router-dom";
import styled, {keyframes} from "styled-components";
import Ship from '../../../../assets/ship.png';
import Favorite from "../../../../assets/favorite.svg";
import favoriteEmpty from "../../../../assets/favorite_broder.svg";
import targetImage from "../../../../assets/asteroid.png";
import enemyImage from "../../../../assets/fire_asteroid.png";
import $ from "jquery";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/damage.ogg";

// Movement speed
const ms = 4;
let keys = {};

function CanvasAsteroidsGame() {
    let invincible = false;
    const [numberOfLives, setNumberOfLives] = useState(3);
    const [isGameOver, setIsGameOver] = useState(false);
    const [gameWon, setGameWon] = useState(false);

    function fallingAsteroids(asteroidsList) {
        let asteroidNumber = Math.floor(Math.random() * 12);
        if (asteroidsList[asteroidNumber] !== undefined && !asteroidsList[asteroidNumber].classList.contains("is-falling")) {
            asteroidsList[asteroidNumber].classList.add("is-falling");
            if (asteroidsList[asteroidNumber] !== undefined) {
                setTimeout(() => {
                    asteroidsList[asteroidNumber]?.classList.remove("is-falling");
                }, 3000);
            }
        }
    }

    function handleCollision(player, asteroid, direction) {
        let objectPlayer = player.getBoundingClientRect();
        let objectAsteroid = asteroid.getBoundingClientRect();

        if (asteroid === undefined) {
            return true
        }
        switch (direction) {
            case "37": // Left arrow key
                if (
                    // Checking the position of my objects to predict a collision
                    objectPlayer.left - 66 < objectAsteroid.left + objectAsteroid.width &&
                    objectPlayer.left - 66 + objectPlayer.width > objectAsteroid.left &&
                    objectPlayer.top < objectAsteroid.top + objectAsteroid.height &&
                    objectPlayer.top + objectPlayer.height > objectAsteroid.top
                ) {
                    return true;
                }
                return false;

            case "39": // Right arrow key
                if (
                    objectPlayer.left + 66 < objectAsteroid.left + objectAsteroid.width &&
                    objectPlayer.left + 66 + objectPlayer.width > objectAsteroid.left &&
                    objectPlayer.top < objectAsteroid.top + objectAsteroid.height &&
                    objectPlayer.top + objectPlayer.height > objectAsteroid.top
                ) {
                    return true;
                }
                return false;

            default:
                if (
                    objectPlayer.left < objectAsteroid.left + objectAsteroid.width &&
                    objectPlayer.left + objectPlayer.width > objectAsteroid.left &&
                    objectPlayer.top < objectAsteroid.top + objectAsteroid.height &&
                    objectPlayer.top + objectPlayer.height > objectAsteroid.top
                ) {
                    return true;
                }
                return false;
        }

    }

    function handleLives() {
        setNumberOfLives((numberOfLives) => numberOfLives - 1);
    }

    useEffect(() => {
        if(numberOfLives > 1 ) {
            setTimeout(() => {
                setGameWon(true);
            }, 30000);
        }
        return () => {
            clearTimeout();
        };
    }, []);

    useEffect(() => {
        if (numberOfLives < 1) {
            setIsGameOver(true);
        }
    }, [numberOfLives]);

    useEffect(() => {
        $(document).on("keydown", function (e) {
            switch (e.keyCode) {
                case 65:
                    keys[37] = true;
                    break;
                case 83:
                    keys[40] = true;
                    break;
                case 68:
                    keys[39] = true;
                    break;
                case 87:
                    keys[38] = true;
                    break;
                default:
                    keys[e.keyCode] = true;
                    break;
            }
        });

        // Remove the key in "keys" when not pressed anymore
        $(document).on("keyup", function (e) {
            switch (e.keyCode) {
                case 65:
                    delete keys[37];
                    break;
                case 83:
                    delete keys[40];
                    break;
                case 68:
                    delete keys[39];
                    break;
                case 87:
                    delete keys[38];
                    break;
                default:
                    delete keys[e.keyCode];
                    break;
            }
        });

        let myAsteroidsList = document.getElementsByClassName("asteroid");
        let MainPlayer = document.getElementById("player");
        let Canvas = document.getElementById("canvas-asteroids");

        const moveplayer = () => {
            let player = $("#player");
            if (!invincible) {
                for (const asteroid of myAsteroidsList) {
                    if (handleCollision(MainPlayer, asteroid)) {
                        invincible = true;
                        player.addClass('collision');
                        handleLives();
                        setTimeout(function () {
                            invincible = false;
                            player.removeClass('collision');
                        }, 1200);
                    }
                }
            }
            for (let direction in keys) {
                if (!keys.hasOwnProperty(direction)) continue;

                let objectPlayer = MainPlayer.getBoundingClientRect();

                //left
                if (direction == 37) {
                    let movementpx = "-=" + ms;

                    if(!handleCollision(MainPlayer,Canvas,"37")){
                        movementpx = "-=0";
                    }

                    player.animate({left: movementpx}, 0);
                }
                //right
                if (direction == 39) {
                    let movementpx = "+=" + ms;

                    if(!handleCollision(MainPlayer,Canvas,"39")){
                        movementpx = "+=0";
                    }

                    player.animate({left: movementpx}, 0);
                }
            }
        };
        const frame = setInterval(moveplayer, 20);

        let asteroidsList = document.getElementsByClassName("asteroid");
        const falling = setInterval(() => {
            fallingAsteroids(asteroidsList);
        }, 500);

        return () => {
            clearInterval(frame);
            clearInterval(falling);
            clearTimeout();
        };
    }, []);


    return (
        <Flexbox>
            <StyledBoard id="canvas-asteroids">
                <Enemy>
                    <img src={enemyImage} alt=""/>
                </Enemy>
                <Asteroids>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-0"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-1"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-2"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-3"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-4"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-5"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-6"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-7"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-8"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-9"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-10"/>
                    <img src={targetImage} alt="" className="asteroid" id="asteroid-11"/>
                </Asteroids>
                <Player>
                    <img src={Ship} alt="" id="player"/>
                </Player>
            </StyledBoard>
            <div className="player__lives">
                {numberOfLives === 1 &&
                <div className="lives">
                    <ReactAudioPlayer
                        src={Music}
                        controls={false}
                        autoPlay={true}
                    />
                    <img src={Favorite} alt=""/>
                    <img src={favoriteEmpty} alt=""/>
                    <img src={favoriteEmpty} alt=""/>
                </div>
                }
                {numberOfLives === 2 &&
                <div className="lives">
                    <ReactAudioPlayer
                        src={Music}
                        controls={false}
                        autoPlay={true}
                    />
                    <img src={Favorite} alt=""/>
                    <img src={Favorite} alt=""/>
                    <img src={favoriteEmpty} alt=""/>
                </div>
                }
                {numberOfLives === 3 &&
                <div className="lives">
                    <img src={Favorite} alt=""/>
                    <img src={Favorite} alt=""/>
                    <img src={Favorite} alt=""/>
                </div>
                }

            </div>
            {isGameOver && <Redirect to='/asteroidsgame-over'/>}
            {gameWon && <Redirect to='/asteroidsgame-won'/>}
        </Flexbox>
    );
}

const Flexbox = styled.div`
  display: flex;
  flex-direction: column;

  .player__lives {
    margin-top: 70px;

    .lives {
      img {
        width: 20px;
      }
    }
  }
`;

const StyledBoard = styled.section`
  background: ${(props) => props.theme.transparentWhite90};
  -webkit-box-shadow: 0 0 6px 0 ${(props) => props.theme.transparentWhite};
  box-shadow: 0 0 6px 0 ${(props) => props.theme.transparentWhite};
  border: 1px solid ${(props) => props.theme.transparentWhite};
  border-radius: 20px;
  width: 60rem;
  height: 30rem;
  position: relative;
  top: 50px;
`;

const Enemy = styled.div`
  position: absolute;
  left: 50%;
  top: -100px;
  transform: translate(-50%);
  width: 10rem;
  z-index: 999;

  img {
    width: 100%;
  }
`;

const bouncingAnimation = keyframes`
  0% {
    transform: scale(1, 1) translateY(0);
    opacity: 1;
  }
  10% {
    transform: scale(1.1, .9) translateY(0);
    opacity: 0.5;
  }
  30% {
    transform: scale(.9, 1.1) translateY(-60px);
    opacity: 1;
  }
  50% {
    transform: scale(1.05, .95) translateY(0);
    opacity: 1;
  }
  57% {
    transform: scale(1, 1) translateY(-7px);
    opacity: 1;
  }
  64% {
    transform: scale(1, 1) translateY(0);
    opacity: 1;
  }
  100% {
    transform: scale(1, 1) translateY(0);
    opacity: 1;
  }
`

const fallingAcornAnimation = keyframes`
  0% {
    -webkit-transform: rotate(0);
    -webkit-transform-origin: 50% 0;
    transform: rotate(0);
    transform-origin: 50% 0;
  }

  10% {
    -webkit-transform: rotate(2deg);
    transform: rotate(2deg);
  }

  20% {
    -webkit-transform: rotate(-4deg);
    transform: rotate(-4deg);
  }

  30% {
    -webkit-transform: rotate(4deg);
    transform: rotate(4deg);
  }

  40% {
    -webkit-transform: rotate(-4deg);
    transform: rotate(-4deg);
  }

  50% {
    -webkit-transform: rotate(4deg);
    transform: rotate(4deg);
  }

  60% {
    -webkit-transform: rotate(-4deg);
    transform: rotate(-4deg);
  }

  70% {
    -webkit-transform: rotate(4deg);
    transform: rotate(4deg);
  }

  80% {
    -webkit-transform: rotate(-2deg);
    transform: rotate(-2deg);
  }

  90% {
    -webkit-transform: rotate(2deg);
    transform: rotate(2deg);
  }

  100% {
    -webkit-transform: rotate(2deg);
    -webkit-transform-origin: 50% 0;
    transform: rotate(2deg);
    transform-origin: 50% 0;
  }
`;

const Asteroids = styled.div`
  position: relative;
  top: -100px;
  display: flex;
  width: 100%;
  height: 100%;

  img {
    position: relative;
    top: 0;
    width: 5rem;
    height: min-content;
    visibility: hidden;

    &.is-falling {
      visibility: initial;
      top: 50rem;
      transition: 3s linear;
      animation: ${fallingAcornAnimation} 3s infinite linear;
    }
  }
`;

const Player = styled.div`
  width: 100%;
  
  .collision {
    animation: ${bouncingAnimation} 2s infinite cubic-bezier(0.280, 0.840, 0.420, 1);
  }

  img {
    width: 4rem;
    height: auto;
    position: absolute;
    bottom: 0;
    left: ${(props) => props.positionPlayer};
  }
`;

export default CanvasAsteroidsGame;
