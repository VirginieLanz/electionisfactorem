import React from 'react';
import styled from 'styled-components';
import backgroundImage from "../../../../assets/scene-04.png";
import CanvasAsteroidsGame from "./CanvasAsteroidsGame";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/space.wav";

function AsteroidsGame() {

    return (
        <Content className="asteroids-game">
            <div className="background"><img src={backgroundImage} alt=""/></div>
            <div className="canvas">
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <CanvasAsteroidsGame/>
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    filter: brightness(0.4);

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }

  & .canvas {
    display: flex;
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: center;
  }
`


export default AsteroidsGame;
