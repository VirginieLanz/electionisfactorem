import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import backgroundImage from "../../../../assets/scene-01.png";
import HeroTitle from "../../../Hero/HeroTitle";
import HeroSubtitle from "../../../Hero/HeroSubtitle";
import secondCharacter from "../../../../assets/secondcharacter.png";
import ArrowDown from "../../../../assets/arrow-down-w.png";
import DialogueComponent from "../../../TextsComponents/DialogueComponent";
import ButtonArrow from "../../../Button/ButtonArrow";
import NarrationComponent from "../../../TextsComponents/NarrationComponent";
import Button from "../../../Button/Button";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/forest.wav";

function Choice2() {
    const content = {
        backgroundUrl: '',
        title: 'Chapitre 1',
        subtitle: 'Une rencontre improbable',
    }

    const steps1 = [
        "Krriak... Truum truum",
        "...",
        "Ha pardon, je réglais mon appareil de communication ! Tu devrais mieux me comprendre maintenant.",
        "Je m'appelle Lena, je viens du cosmos. \nJ'ai eu un accident et...",
        "... Je ne me souviens plus trop. \nOn est-où, là ?",
        "Ha ! Sur terre... d'accord. \nEffectivement, ce n'est pas trop la destination que j'espérais atteindre. Mais la température est agréable. Et ces créatures-là, qui chantent, me ressemblent étrangement. \n\n Oh, excuse-moi ! \n je ne t'ai même pas demandé ton prénom ?",
        "Enchantée, Jules. \nJe suis heureuse de tomber sur toi"
    ];

    const [finished, setFinished] = useState(false);
    const [selected, setSelected] = useState(0);

    const handleClick = () => {
        setSelected(prev => {
            if (prev === steps1.length - 1) {
                setFinished(true);
            } else {
                return prev + 1;
            }
        });
    };

    return (
        <Content className="level1">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="container">
                <HeroTitle title={content.title}/>
                <HeroSubtitle subtitle={content.subtitle}/>
                <MainCharacter src={secondCharacter} alt=""/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <Story>
                    <div>
                        {finished ?
                            <>
                                <NarrationComponent
                                    text={'Tu as vraiment fait une drôle de rencontre. Elle a l\'air sympa, cette Lena. \nMême si, entre nous, tu aurais bien aimé avoir des ailes toi aussi...'}/>
                                <Button class="secondaryButton" text="Passer au chapitre 2" link="/chapter-2"/>
                            </>
                            :
                            <>
                                <DialogueComponent text={steps1[selected]}
                                                   buttonContent={
                                                       <ButtonArrow
                                                           onClick={handleClick}
                                                           imageUrl={ArrowDown}/>
                                                   }/>
                            </>
                        }
                    </div>
                </Story>
            </Container>
        </Content>
    );
}


const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`


const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    background: black;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
      animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.3s both;
    }
  }
`


const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`

const Story = styled.div`
  min-width: 80%;
  max-width: 80%;

  a {
    color: ${props => props.theme.white};
  }

  .hidden {
    display: none;
  }

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }
`

const MainCharacter = styled.img`
  animation-delay: 0.5s;
  width: 300px;
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  position: absolute;
  bottom: 100px;
  right: 10%;
`

export default Choice2;
