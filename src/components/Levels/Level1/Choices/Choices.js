import React from 'react';
import styled from 'styled-components';
import ChoiceComponent from "../../../ChoiceComponent/ChoiceComponent";

function Choices() {
    return (
        <Content className="choices">
            <ChoiceComponent link="/level-1-choice-1" title="Crier et partir en pleurant"/>
            <ChoiceComponent link="/level-1-choice-2" title="Tendre la main et essayer de communiquer avec la créature"/>
            <ChoiceComponent link="/level-1-choice-3" title="L’assommer avec un bâton pour qu’elle ne bouge plus"/>
        </Content>
    );
}

const Content = styled.div`
  display: flex;
  justify-content: center;
  gap: 30px;
`;


export default Choices;
