import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import backgroundImage from "../../../../assets/scene-01.png";
import HeroTitle from "../../../Hero/HeroTitle";
import HeroSubtitle from "../../../Hero/HeroSubtitle";
import mainCharacter from "../../../../assets/maincharacter_runaway.png";
import NarrationComponent from "../../../TextsComponents/NarrationComponent";
import Button from "../../../Button/Button";
import ArrowRight from "../../../../assets/arrow-right-w.png";
import ButtonArrow from "../../../Button/ButtonArrow";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/forest.wav";

function Choice1() {

    const content = {
        backgroundUrl: '',
        title: 'Chapitre 1',
        subtitle: 'Une rencontre improbable',
    }

    const steps1 = [
        "*Huff... huff... huff*",
        "Tu as fui dans la forêt le plus vite possible... Non mais sérieusement, c'était quoi cette créature ?",
        "Tu pensais être un aventurier mais là... C'est trop ! Tu ne semble pas prêt à endosser ce si grand rôle.",
        "Malheureusement pour toi, ta course effrénée t'a mené tout droit vers une maman sanglier..."
    ];

    const [selected, setSelected] = useState(0);
    const [finished, setFinished] = useState(false);

    const handleClick = () => {
        setSelected(prev => {
            if (prev === steps1.length - 1) {
                setFinished(true);
                let paragraph = document.getElementById("narration-01");
                paragraph.classList.add('hidden');
            } else {
                return prev + 1;
            }
        });
    };

    return (
        <Content className="level1">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="container">
                <HeroTitle title={content.title}/>
                <HeroSubtitle subtitle={content.subtitle}/>
                <MainCharacter src={mainCharacter} alt=""/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <Story>
                    <div>
                        {finished ?
                            <>
                                <NarrationComponent
                                    text={'Elle n\'aime pas trop te voir près de ses marcassins. \nPrépare-toi à devoir te battre pour te sauver !'}/>
                                <Button text="Se défendre" class="secondaryButton" link="/mini-boargame"/>
                            </>
                            :
                                <div id="narration-01">
                                    <NarrationComponent text={steps1[selected]}
                                                        buttonContent={
                                                            <ButtonArrow
                                                                onClick={handleClick}
                                                                imageUrl={ArrowRight}/>
                                                        }/>
                                </div>
                        }
                    </div>
                </Story>
            </Container>
        </Content>
    );
}


const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`


const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    background: black;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
      animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.3s both;
    }
  }
`

const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`


const Story = styled.div`
  min-width: 80%;
  max-width: 80%;

  .hidden {
    display: none;
  }

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }
`

const MainCharacter = styled.img`
  animation-delay: 1s;
  width: 350px;
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  position: absolute;
  bottom: 70px;
  right: 10%;
`

export default Choice1;
