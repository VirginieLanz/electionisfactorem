import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import backgroundImage from "../../../../assets/scene-01.png";
import HeroTitle from "../../../Hero/HeroTitle";
import HeroSubtitle from "../../../Hero/HeroSubtitle";
import secondCharacter from "../../../../assets/secondcharacter_angry.png";
import ArrowDown from "../../../../assets/arrow-down-w.png";
import NarrationComponent from "../../../TextsComponents/NarrationComponent";
import ButtonArrow from "../../../Button/ButtonArrow";
import Button from "../../../Button/Button";
import DialogueComponent from "../../../TextsComponents/DialogueComponent";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/forest.wav";

function Choice3() {
    const content = {
        backgroundUrl: '',
        title: 'Chapitre 1',
        subtitle: 'Une rencontre improbable',
    }

    const steps1 = [
        "Kurrk kurrk kurrk !",
        "Eh ! Je viens de régler mon système de communication. \nTu devrais me comprendre, dorénavant !",
        "T'as essayé de m'assommer ! Je l'ai bien vu ! \nC'est quoi, ces manières ? Ce n'est pas gentil, du tout !",
        "Je vais te montrer ce que ça va t'apporter d'être mal-poli avec moi !"
    ];

    const [finished, setFinished] = useState(false);
    const [selected, setSelected] = useState(0);

    const handleClick = () => {
        setSelected(prev => {
            if (prev === steps1.length - 1) {
                setFinished(true);
            } else {
                return prev + 1;
            }
        });
    };

    return (
        <Content className="level1">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="container">
                    <HeroTitle title={content.title}/>
                    <HeroSubtitle subtitle={content.subtitle}/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <SecondCharacter className="second-character" src={secondCharacter} alt=""/>
                <Story>
                    <div>
                        {finished ?
                            <>
                                <NarrationComponent text={'Oups, on dirait que ce n\'était pas la bonne décision à prendre. Elle a vraiment l\'air en colère. \nJe ferai attention, si j\'étais toi.'}/>
                                <Button text="Se défendre" class="secondaryButton" link="/mini-feathergame"/>
                            </>
                            :
                                <DialogueComponent text={steps1[selected]}
                                                    buttonContent={
                                                        <ButtonArrow
                                                            onClick={handleClick}
                                                            imageUrl={ArrowDown}/>
                                                    }/>
                        }
                    </div>
                </Story>
            </Container>
        </Content>
    );
}


const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    background: black;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
      animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.3s both;
    }
  }
`



const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`


const Story = styled.div`
  min-width: 80%;
  max-width: 80%;
  .hidden {
    display: none;
  }

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }
`


const SecondCharacter = styled.img`
  animation-delay: 1s;
  width: 300px;
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  position: absolute;
  bottom: 70px;
  right: 10%;
`
export default Choice3;
