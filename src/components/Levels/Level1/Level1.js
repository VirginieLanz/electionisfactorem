import React, {useState} from "react";
import styled, {keyframes} from 'styled-components';
import HeroTitle from "../../Hero/HeroTitle";
import HeroSubtitle from "../../Hero/HeroSubtitle";
import backgroundImage from "../../../assets/scene-01.png";
import ArrowRight from "../../../assets/arrow-right-w.png";
import ArrowDown from "../../../assets/arrow-down-w.png";
import mainCharacter from "../../../assets/maincharacter.png";
import secondCharacter from "../../../assets/secondcharacter.png";
import Choices from "./Choices/Choices";
import ButtonArrow from "../../Button/ButtonArrow";
import DialogueComponent from "../../TextsComponents/DialogueComponent";
import NarrationComponent from "../../TextsComponents/NarrationComponent";
import ReactAudioPlayer from 'react-audio-player';
import Music from "../../../assets/forest.wav";


function Level1() {
    const content = {
        backgroundUrl: '',
        title: 'Chapitre 1',
        subtitle: 'Une rencontre improbable',
        arrowRight: ArrowRight,
        arrowDown: ArrowDown
    }

    const storyIntro = [
        "Bonjour aventurier, \n te voici au coeur de la forêt Heule, près d'un petit village nommé Lauld...\n\n La clairière laisse passer de nombreux rayons de soleil. \n Il fait beau et la température de cette matinée d'été s'élève peu à peu. ",
        "Tu entends, extasié, les oiseaux qui chantent au loin. \nTu entends même, parfois, un animal sauvage qui se déplace, d'un pas léger, vers les odeurs enivrantes qu'il renifle.",
        "Aujourd'hui te semble être la journée idéale pour explorer le monde. \n Alors tu as, comme à chaque fois, noué tes lacets dans l'espoir de faire un jour, la rencontre de ta vie. \n\nCe moment n'est encore jamais arrivé.",
    ];

    const steps1 = [
        "Sauf aujourd'hui, peut-être...",
        "Ouah ! Je me sens en pleine forme ce matin ! J'ai bien fait d'être parti à l'aube aujourd'hui.\n\nIl paraît que l'avenir appartient à ceux qui se lèvent tôt... Mais bon, on ne m'a jamais donné d'heure précise.",
        "Cependant, des craquements t'interpellent et te sortent de ta rêverie. Des sons de plus en plus forts se font entendre. ",
        "*Krr Krr*",
        "Qu'est-ce que c'était que ces bruits ?!",
    ];

    const steps2 = [
        "Kurrk kurrk... \n Olede luker dae ? Hurr deroa koale... ",
        "Ainke tok e... \n Koellek akode hirel ? \n Koellek akode laede !",
    ];

    const storyChoices = [
        "Tu n'as jamais rien vu de semblable auparavant, même après avoir explorer les environs de ton village de fond en comble... \n\n Quelle sera ta réaction face à cette rencontre inattendue ?"
    ];


    const [characterIsTalking, setCharacterIsTalking] = useState(false);
    const [displayCharacter, setDisplayCharacter] = useState(false);
    // Introduction
    const [storyIntroSelected, setStoryIntroSelected] = useState(0);
    const [introFinished, setIntroFinished] = useState(false);
    // Steps 1
    const [steps1Selected, setSteps1Selected] = useState(0);
    const [steps1Started, setSteps1Started] = useState(false);
    const [steps1Finished, setSteps1Finished] = useState(false);
    // Steps 2
    const [steps2Selected, setSteps2Selected] = useState(0);
    const [steps2Started, setSteps2Started] = useState(false);
    const [steps2Finished, setSteps2Finished] = useState(false);

    const handleNavigationIntro = () => {
        setStoryIntroSelected(prev => {
            if (prev === storyIntro.length - 1) {
                setIntroFinished(true);
                setSteps1Started(true);
            } else {
                return prev + 1;
            }
        });
    };

    const handleNavigationSteps1 = () => {
        setSteps1Selected(prev => {
            if (prev === steps1.length - 1) {
                setSteps1Finished(true);
                setSteps2Started(true);
            } else {
                if (prev === steps1.length - 5) {
                    setDisplayCharacter(true);
                }
                if (prev === steps1.length - 2 || prev === steps1.length - 5) {
                    setCharacterIsTalking(true);
                } else {
                    setCharacterIsTalking(false);
                }
                return prev + 1;
            }
        });
    };

    const handleNavigationSteps2 = () => {
        setSteps2Selected(prev => {
            if (prev === steps2.length - 1) {
                setSteps2Finished(true);
                let divs = document.querySelectorAll('.character');
                for (let i = 0; i < divs.length; i++) {
                    divs[i].classList.add('hidden');
                }
                setCharacterIsTalking(true);
            } else {
                setCharacterIsTalking(true);
                return prev + 1;
            }
        });
    };


    return (
        <Content className="level1">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="chapters__intro container">
                <HeroTitle title={content.title}/>
                <HeroSubtitle subtitle={content.subtitle}/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <Story>
                        {introFinished ?
                            <div>
                                {displayCharacter &&
                                <Characters className="character main-character" src={mainCharacter} alt=""/>
                                }
                                {steps1Started &&
                                <div>
                                    {steps1Finished ?
                                        <>
                                            <Characters className="character second-character" src={secondCharacter}
                                                        alt=""/>
                                            {steps2Started &&
                                            <div className="story__third-section">
                                                {steps2Finished ?
                                                    <>
                                                        <NarrationComponent text={storyChoices}/>
                                                        <Choices/>
                                                    </>
                                                    :
                                                    <>
                                                        {characterIsTalking &&
                                                        <DialogueComponent text={steps2[steps2Selected]}
                                                                           buttonContent={
                                                                               <ButtonArrow
                                                                                   onClick={handleNavigationSteps2}
                                                                                   imageUrl={ArrowDown}/>
                                                                           }
                                                        />
                                                        }
                                                    </>
                                                }
                                            </div>
                                            }
                                        </>
                                        :
                                        <>
                                            {characterIsTalking ?
                                                <DialogueComponent text={steps1[steps1Selected]}
                                                                   buttonContent={
                                                                       <ButtonArrow
                                                                           onClick={handleNavigationSteps1}
                                                                           imageUrl={ArrowDown}/>
                                                                   }
                                                />
                                                :
                                                <NarrationComponent text={steps1[steps1Selected]}
                                                                    buttonContent={
                                                                        <ButtonArrow
                                                                            onClick={handleNavigationSteps1}
                                                                            imageUrl={ArrowRight}/>
                                                                    }
                                                />
                                            }
                                        </>
                                    }
                                </div>
                                }
                            </div>
                            :
                            <NarrationComponent text={storyIntro[storyIntroSelected]}
                                                buttonContent={
                                                    <ButtonArrow
                                                        onClick={handleNavigationIntro}
                                                        imageUrl={ArrowRight}/>
                                                }
                            />
                        }
                </Story>
            </Container>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
`

const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`
const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`

const disparitionAnimation = keyframes`
  0% {
    translateZ(0);
    opacity: 1;
  }
  100% {
    translateZ(80px);
    opacity: 0;
  }
`

const Story = styled.div`
  min-width: 80%;
  max-width: 80%;

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }

  .hidden {
    animation: ${disparitionAnimation} 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  }
`


const Characters = styled.img`
  animation-delay: 1s;
  width: 350px;
  animation: ${apparitionAnimation} 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
`

export default Level1;
