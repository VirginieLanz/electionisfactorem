import React, {useState} from "react";
import styled, {keyframes} from 'styled-components';
import HeroTitle from "../../Hero/HeroTitle";
import HeroSubtitle from "../../Hero/HeroSubtitle";
import backgroundImage from "../../../assets/scene-01.png";
import ArrowRight from "../../../assets/arrow-right-w.png";
import ArrowDown from "../../../assets/arrow-down-w.png";
import mainCharacter from "../../../assets/maincharacter.png";
import secondCharacter from "../../../assets/secondcharacter.png";
import Choices from "./Choices/Choices";
import ButtonArrow from "../../Button/ButtonArrow";
import DialogueComponent from "../../TextsComponents/DialogueComponent";
import NarrationComponent from "../../TextsComponents/NarrationComponent";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../assets/light-rain.wav";


function Level2() {
    const content = {
        backgroundUrl: '',
        title: 'Chapitre 2',
        subtitle: 'Une voyage de souvenirs',
        arrowRight: ArrowRight,
        arrowDown: ArrowDown
    }

    const storyIntro = [
        "Ploc... ploc... ploc... \n Quelques gouttes tombes sur ton front. Il a commencé à pleuvoir, doucement. Comme une légère pluie d'été.",
        "Tu n'en reviens pas. Tu viens de faire une rencontre totalement improbable... qui l'aurait cru ? Même si tu es un aventurier dans l'âme, jamais, ôh grand jamais, tu ne pensais pouvoir un jour vivre cette expérience. ",
        "Tu l'observe, interloqué. Qui peut-elle être ? Et d'où vient-elle ? Pourquoi peut-elle parler ta langue alors qu'elle ne semblait pas pouvoir le faire lors de vos premiers mots échangés ?"
    ];

    const steps1 = [
        "Alors, plein d'enthousiasme, des idées se bousculent dans ta tête.",
        "C'est incroyable, ce que je vis là. Il faut que je prenne des notes, absolument ! Ou que je prenne un téléphone, pour faire un reportage, comme à la télévision.",
        "Elle aussi, te regarde, curieuse. Elle voit bien que tu penses à pleins de choses. Elle a l'air de s'en amuser.",
        "Tu sembles être différent de ce qu'elle connaît également. Tout semble différent, pour elle, à priori. Elle regarde autour d'elle, curieuse, et soucieuse.",
        "Alors, si je comprend bien, tu t'appelles Lena... C'est sympa comme prénom. Par contre, comment fais-tu pour communiquer avec moi ? Tu as parlé d'un appareil de communication... Qu'est-ce que c'est, exactement ?",
    ];

    const steps2 = [
        "Oui, c'est juste, c'est bien mon appareil de communication qui nous permet de discuter comme on le fait actuellement. Tu as une bonne mémoire !",
        "Il s'agit d'une antenne cosmique qui permet de récupérer les ondes émises lors d'une prise de parole. Ces ondes sont ensuite analysées par l'appareil et traduites instantanément. C'est génial, non ?",
    ];

    const storyChoices = [
        "Lena semble ouverte à la discussion. Elle n'a pas hésité à te répondre. Profites-en pour en savoir plus sur elle ! Quel est le sujet sur lequel tu aimerais en apprendre d'avantages ?"
    ];


    const [characterIsTalking, setCharacterIsTalking] = useState(false);
    // Introduction
    const [storyIntroSelected, setStoryIntroSelected] = useState(0);
    const [introFinished, setIntroFinished] = useState(false);
    // Steps 1
    const [steps1Selected, setSteps1Selected] = useState(0);
    const [steps1Started, setSteps1Started] = useState(false);
    const [steps1Finished, setSteps1Finished] = useState(false);
    // Steps 2
    const [steps2Selected, setSteps2Selected] = useState(0);
    const [steps2Started, setSteps2Started] = useState(false);
    const [steps2Finished, setSteps2Finished] = useState(false);

    const handleNavigationIntro = () => {
        setStoryIntroSelected(prev => {
            if (prev === storyIntro.length - 1) {
                setIntroFinished(true);
                setSteps1Started(true);
            } else {
                return prev + 1;
            }
        });
    };

    const handleNavigationSteps1 = () => {
        setSteps1Selected(prev => {
            if (prev === steps1.length - 1) {
                setSteps1Finished(true);
                setSteps2Started(true);
            } else {
                if (prev === steps1.length - 2 || prev === steps1.length - 5) {
                    setCharacterIsTalking(true);
                } else {
                    setCharacterIsTalking(false);
                }
                return prev + 1;
            }
        });
    };

    const handleNavigationSteps2 = () => {
        setSteps2Selected(prev => {
            if (prev === steps2.length - 1) {
                setSteps2Finished(true);
                let divs = document.querySelectorAll('.character');
                for (let i = 0; i < divs.length; i++) {
                    divs[i].classList.add('hidden');
                }
                setCharacterIsTalking(true);
            } else {
                setCharacterIsTalking(true);
                return prev + 1;
            }
        });
    };


    return (
        <Content className="level2">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="chapters__intro container">
                <HeroTitle title={content.title}/>
                <HeroSubtitle subtitle={content.subtitle}/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <Story>
                    <Characters className="character main-character" src={mainCharacter} alt=""/>
                    <Characters className="character second-character" src={secondCharacter}
                                alt=""/>
                    {introFinished ?
                        <div>
                            {steps1Started &&
                            <div>
                                {steps1Finished ?
                                    <>
                                        {steps2Started &&
                                        <div className="story__third-section">
                                            {steps2Finished ?
                                                <>
                                                    <NarrationComponent text={storyChoices}/>
                                                    <Choices/>
                                                </>
                                                :
                                                <>
                                                    {characterIsTalking &&
                                                    <DialogueComponent text={steps2[steps2Selected]}
                                                                       buttonContent={
                                                                           <ButtonArrow
                                                                               onClick={handleNavigationSteps2}
                                                                               imageUrl={ArrowDown}/>
                                                                       }
                                                    />
                                                    }
                                                </>
                                            }
                                        </div>
                                        }
                                    </>
                                    :
                                    <>
                                        {characterIsTalking ?
                                            <DialogueComponent text={steps1[steps1Selected]}
                                                               buttonContent={
                                                                   <ButtonArrow
                                                                       onClick={handleNavigationSteps1}
                                                                       imageUrl={ArrowDown}/>
                                                               }
                                            />
                                            :
                                            <NarrationComponent text={steps1[steps1Selected]}
                                                                buttonContent={
                                                                    <ButtonArrow
                                                                        onClick={handleNavigationSteps1}
                                                                        imageUrl={ArrowRight}/>
                                                                }
                                            />
                                        }
                                    </>
                                }
                            </div>
                            }
                        </div>
                        :
                        <NarrationComponent text={storyIntro[storyIntroSelected]}
                                            buttonContent={
                                                <ButtonArrow
                                                    onClick={handleNavigationIntro}
                                                    imageUrl={ArrowRight}/>
                                            }
                        />
                    }
                </Story>
            </Container>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
`

const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`
const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`

const disparitionAnimation = keyframes`
  0% {
    translateZ(0);
    opacity: 1;
  }
  100% {
    translateZ(80px);
    opacity: 0;
  }
`

const Story = styled.div`
  min-width: 80%;
  max-width: 80%;

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }

  .hidden {
    animation: ${disparitionAnimation} 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  }
`


const Characters = styled.img`
  animation-delay: 1s;
  width: 350px;
  animation: ${apparitionAnimation} 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
`

export default Level2;
