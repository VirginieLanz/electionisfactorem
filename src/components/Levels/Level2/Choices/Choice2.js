import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import backgroundImage from "../../../../assets/scene-01.png";
import HeroTitle from "../../../Hero/HeroTitle";
import HeroSubtitle from "../../../Hero/HeroSubtitle";
import secondCharacter from "../../../../assets/secondcharacter.png";
import ArrowDown from "../../../../assets/arrow-down-w.png";
import DialogueComponent from "../../../TextsComponents/DialogueComponent";
import ButtonArrow from "../../../Button/ButtonArrow";
import NarrationComponent from "../../../TextsComponents/NarrationComponent";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../../../assets/light-rain.wav";
import ChoiceComponent from "../../../ChoiceComponent/ChoiceComponent";

function Choice2() {
    const content = {
        backgroundUrl: '',
        title: 'Chapitre 2',
        subtitle: 'Un voyage de souvenirs',
    }

    const steps1 = [
        "J'habite sur la planète de Koaudk, située à 50 années lumière de la planète Terre.",
        "Je vois qu'ici vous n'avez qu'une lune. Nous, nous en avons 7.",
        "Il y a beaucoup de monde chez moi, pleins de Dreok, comme moi. Beaucoup voyagent, mais je suis la seule dans ma famille.",
        "Normalement, ils devraient avoir reçu un signal comme quoi je suis ici.",
        "Il faut que je retrouve mon vaisseau. Je ne devrai pas être là."
    ];

    const [finished, setFinished] = useState(false);
    const [selected, setSelected] = useState(0);

    const handleClick = () => {
        setSelected(prev => {
            if (prev === steps1.length - 1) {
                setFinished(true);
            } else {
                return prev + 1;
            }
        });
    };

    return (
        <Content className="level2">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <Container className="container">
                <HeroTitle title={content.title}/>
                <HeroSubtitle subtitle={content.subtitle}/>
                <MainCharacter src={secondCharacter} alt=""/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                    loop={true}
                />
                <Story>
                    <div>
                        {finished ?
                            <div className="center">
                                <NarrationComponent
                                    text={'Tu as appris où habitait Lena, et qu\'elle n\'avait pas prévu de s\'arrêter sur Terre. Qu\'a-t-il bien pu se passer ?' }/>
                                <ChoiceComponent link="/level-2-choice-3" title="Lui demander ce qu'elle fait ici"/>
                            </div>
                            :
                            <>
                                <DialogueComponent text={steps1[selected]}
                                                   buttonContent={
                                                       <ButtonArrow
                                                           onClick={handleClick}
                                                           imageUrl={ArrowDown}/>
                                                   }/>
                            </>
                        }
                    </div>
                </Story>
            </Container>
        </Content>
    );
}


const apparitionAnimation = keyframes`
  0% {
    translateZ(80px);
    opacity: 0;
  }
  100% {
    translateZ(0);
    opacity: 1;
  }
`


const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;

  .center {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  
  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    background: black;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
      animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.3s both;
    }
  }
`


const Container = styled.div`
  height: calc(100vh - 100px);
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;

  & .button-sound {
    background: none;
    border: none;
    margin-bottom: 20px;

    img {
      width: 20px;
      cursor: pointer;
    }
  }
`

const Story = styled.div`
  min-width: 80%;
  max-width: 80%;

  a {
    color: ${props => props.theme.white};
  }

  .hidden {
    display: none;
  }

  .main-character {
    position: absolute;
    bottom: 20px;
    left: 10%;
    width: 200px;
  }

  .second-character {
    position: absolute;
    bottom: 70px;
    right: 10%;
    width: 200px;

  }
`

const MainCharacter = styled.img`
  animation-delay: 0.5s;
  width: 300px;
  animation: ${apparitionAnimation} 3s cubic-bezier(0.390, 0.575, 0.565, 1.000) 0.5s both;
  position: absolute;
  bottom: 100px;
  right: 10%;
`

export default Choice2;
