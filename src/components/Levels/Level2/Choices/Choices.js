import React from 'react';
import styled from 'styled-components';
import ChoiceComponent from "../../../ChoiceComponent/ChoiceComponent";

function Choices() {
    return (
        <Content className="choices">
            <ChoiceComponent link="/level-2-choice-1" title="Lui demander de parler de sa famille et de son enfance"/>
            <ChoiceComponent link="/level-2-choice-2" title="Lui demander des détails sur l’endroit où elle habite"/>
            <ChoiceComponent link="/level-2-choice-3" title="Lui demander ce qu'elle fait sur Terre"/>
        </Content>
    );
}

const Content = styled.div`
  display: flex;
  justify-content: center;
  gap: 30px;
`;


export default Choices;
