import React from 'react';
import styled from 'styled-components';
import ButtonRounded from "../Button/ButtonRounded";

function ChapterPreview(props) {
    return (
        <Content className="chapter-preview">
            <Number className="chapter__number">{props.number}</Number>
            <ButtonRounded className="chapter__button" content={props.content} link={props.link}/>
            <Title className="chapter__title">{props.title}</Title>
        </Content>
    );
}

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  color: ${props => props.theme.white};
  
  a {
    margin: 10px 0;
  }
`

const Number = styled.div`
  font-family: ${props => props.theme.limeLight};;
  font-size: ${props => props.theme.medium};
`
const Title = styled.div`
  font-size: ${props => props.theme.extraSmall};
`

export default ChapterPreview;
