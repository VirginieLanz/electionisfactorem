import React from 'react';
import styled from 'styled-components';
import {NavLink} from "react-router-dom";

function ChoiceComponent(props) {
    return (
        <Content className="choice-component">
            <NavLink
                to={props.link}
                className={isActive =>
                    "choice__link" + (!isActive ? "" : "active")
                }
            >
                {props.title}
            </NavLink>
        </Content>
    );
}

const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30%;
  padding: 20px;
  background: ${props => props.theme.transparentWhite};
  border-radius: 20px;
  font-size: ${props => props.theme.small};

  &:active{
    border: 1px solid ${props => props.theme.white};
    -webkit-box-shadow: 0 0 6px 0 ${props => props.theme.white};
    box-shadow: 0 0 6px 0 ${props => props.theme.white};
  }
  
  .choice__link { 
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    text-decoration: none;
    color: ${props => props.theme.white};
  }
`;

export default ChoiceComponent;
