import React from 'react';
import styled from 'styled-components';

function HeroTitle(props) {
    return (
        <Content className="hero__title">
                {props.title}
        </Content>
    );
}

const Content = styled.div`
  font-family: ${props => props.theme.limeLight};
  font-size: ${props => props.theme.extraLarge};
  margin-bottom: 20px;
`;

export default HeroTitle;
