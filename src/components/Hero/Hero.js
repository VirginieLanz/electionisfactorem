import React from 'react';
import styled from 'styled-components';
import Button from "../Button/Button";
import HeroTitle from "./HeroTitle";
import HeroSubtitle from "./HeroSubtitle";
import HeroParagraph from "./HeroParagraph";

function Hero(props) {
    return (
        <Content>
            <div className="hero__background"><img src={props.backgroundUrl} alt=""/></div>
            <div className="hero__content container">
                <HeroTitle title={props.title}/>
                <HeroSubtitle subtitle={props.subtitle}/>
                <HeroParagraph paragraph={props.paragraph}/>
                <Button className="hero__button" content={props.content} text= {props.text} link={props.link}/>
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  top: -50px;
  color: ${props => props.theme.white};

  & .hero__background {
    height: 100vh;
    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
  
  & .hero__content {
    position: relative;
    top: -60vh;
  }
`;

export default Hero;
