import React from 'react';
import styled from 'styled-components';

function HeroParagraph(props) {
    return (
        <Content className="hero__subtitle">
            {props.paragraph}
        </Content>
    );
}

const Content = styled.div`
  font-size: ${props => props.theme.small};
  margin-bottom: 50px;
`;

export default HeroParagraph;
