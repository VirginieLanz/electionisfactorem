import React from 'react';
import styled from 'styled-components';

function HeroSubtitle(props) {
    return (
        <Content className="hero__subtitle">
            {props.subtitle}
        </Content>
    );
}

const Content = styled.div`
  font-size: ${props => props.theme.small};
  margin-top: -20px;
  margin-bottom: 50px;
`;

export default HeroSubtitle;
