import React from 'react';
import Navbar from "../components/Navbar/Navbar";
import Hero from "../components/Hero/Hero";
import backgroundImage from '../assets/scene-03.png';
import styled from 'styled-components';
import ReactAudioPlayer from "react-audio-player";
import Music from "../assets/homepage.wav";

function Homepage() {
    const hero = {
        link: '/chapter-1',
        title: 'Entre dans l\'antre du cosmos d\'Electionis Factorem et découvre un univers prenant et poétique...',
        paragraph: 'Incarne le personnage de Jules, un jeune aventurier de 12 ans qui s\'apprête à faire la découverte de sa vie !\n' +
            'Deviens maître de tes décisions et paie-en les conséquences lorsque ces premières ne seront pas forcément celles qui te mèneront à bien...',
        buttonContent: 'Commencer'
    }
    return (
        <Content className="homepage">
            <Navbar/>
            <ReactAudioPlayer
                src={Music}
                controls={false}
                autoPlay={true}
                loop={true}
            />
            <Hero link={hero.link} backgroundUrl={backgroundImage} title={hero.title} paragraph={hero.paragraph}
                  text={hero.buttonContent}/>
        </Content>
    );
}

const Content = styled.div`
  max-height: 100vh;
  overflow: hidden;
  white-space: pre-line;
`

export default Homepage;
