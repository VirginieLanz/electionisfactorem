import React from 'react';
import styled from 'styled-components';
import backgroundImage from "../../assets/scene-01.png";
import Keys from '../../assets/keys.png';
import ParagraphComponent from "../../components/TextsComponents/ParagraphComponent";

function FeatherGamePage() {
    const content = {
        title: 'Mini-jeu',
        content: "Aventurier un jour, aventurier... pas tous les jours ! \nFaut pas déconner non plus. \nQuoiqu'il en soit, la situation t'a échappé.\n\n Arriveras-tu à t'en sortir ?",
        button: "Commencer",
        imageUrl: Keys,
        link: "/feathergame-play",
        secondContent: "Protège-toi des plumes en te déplaçant sur la droite ou sur la gauche. Tu as trois vies, fais-attention."
    }

    return (
        <Content className="feather-game">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <div className="container center">
                <ParagraphComponent title={content.title} content={content.content} image={content.imageUrl} text={content.button} link={content.link} secondContent={content.secondContent}/>
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    filter: brightness(0.6);

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
  
  & .center {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 60%;
  }
`


export default FeatherGamePage;
