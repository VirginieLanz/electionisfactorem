import React from 'react';
import styled from 'styled-components';
import backgroundImage from "../../assets/scene-04.png";
import Keys from '../../assets/keys.png';
import ParagraphComponent from "../../components/TextsComponents/ParagraphComponent";
import Button from "../../components/Button/Button";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../assets/gameover.wav";

function AsteroidsGameOver() {
    const content = {
        title: 'GAME OVER',
        content: "Mince ! Les astéroïdes ont eu raison de toi !",
        secondContent: "Protège-toi des astéroïdes en te déplaçant sur la droite ou sur la gauche. Tu as trois vies, fais-attention.",
        button: "Recommencer",
        imageUrl: Keys,
        link: "/asteroidsgame-play",
    }

    return (
        <Content className="boar-game">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <div className="container center">
                <ParagraphComponent title={content.title} content={content.content} image={content.imageUrl} text={content.button} link={content.link} secondContent={content.secondContent}/>
                <Button text="Retourner à l'accueil" link="/" class="thirdButton"/>
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                />
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  
  .paragraph-component {
    margin-bottom: 50px;
  }
  

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    filter: brightness(0.6);

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
  
  & .center {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 60%;
  }
`


export default AsteroidsGameOver;

