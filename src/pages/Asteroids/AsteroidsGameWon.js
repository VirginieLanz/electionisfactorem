import React from 'react';
import styled from 'styled-components';
import backgroundImage from "../../assets/scene-04.png";
import Ship from "../../assets/ship.png";
import ParagraphComponent from "../../components/TextsComponents/ParagraphComponent";
import Button from "../../components/Button/Button";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../assets/win.wav";

function AsteroidsGameWon() {
    const content = {
        title: 'Bravo !',
        content: "Tu as réussi à passer le champ d'astéroïdes sans trop de dégâts !",
        secondContent: "Dommage que tu n'aies pas été là lorsque Lena se trouvait en danger... \nTu aurais probablement pu l'aider à s'en sortir.",
        button: "Passer au chapitre 3",
        imageUrl: Ship,
        link: "/chapter-3",
    }

    return (
        <Content className="game-won">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <div className="container center">                <ReactAudioPlayer
                src={Music}
                controls={false}
                autoPlay={true}
            />
                <ParagraphComponent title={content.title} content={content.content} image={content.imageUrl} text={content.button} link={content.link} secondContent={content.secondContent}/>
                <Button text="Retourner à l'accueil" link="/" class="thirdButton"/>
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  
  .paragraph-component {
    margin-bottom: 50px;
  }
  

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    filter: brightness(0.6);

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
  
  & .center {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 60%;
  }
`


export default AsteroidsGameWon;

