import React from 'react';

import styled from 'styled-components';
import Navbar from "../components/Navbar/Navbar";
import HeroTitle from "../components/Hero/HeroTitle";
import Button from "../components/Button/Button";
import HeroParagraph from "../components/Hero/HeroParagraph";
import backgroundImage from "../assets/scene-01.png";
import ReactAudioPlayer from "react-audio-player";
import Music from "../assets/chapter.m4a";

function Chapter1() {
    const content = {
        title: 'Chapitre 1 \n Une rencontre improbable',
        paragraph: '',
        buttonText: 'Commencer',
        link: '/level-1'
    }
    return (
        <Content className="chapter">
            <Navbar/>
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <div className="container">
                <Wrapper>
                    <ReactAudioPlayer
                        src={Music}
                        controls={false}
                        autoPlay={true}
                    />
                    <div className="left-side"></div>
                    <div className="right-side">
                        <HeroTitle title={content.title}/>
                        <HeroParagraph paragraph={content.paragraph}/>
                        <Button className="hero__button" content={content.buttonText} text={content.buttonText} link={content.link}/>
                    </div>
                </Wrapper>
            </div>
        </Content>
    );
}

const Content = styled.div`
  height: 100vh;
  overflow: hidden;
  color: ${props => props.theme.white};
  white-space: pre-line;

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }

  & .container {
    height: 100vh;
    display: flex;
    align-items: center;
  }
`

const Wrapper = styled.div`
  width: 100%;
  display: flex;

  & .left-side,
  & .right-side {
    width: 50%;
  }
`

export default Chapter1;
