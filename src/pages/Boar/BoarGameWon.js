import React from 'react';
import styled from 'styled-components';
import backgroundImage from "../../assets/scene-01.png";
import MainCharacter from "../../assets/maincharacter.png";
import ParagraphComponent from "../../components/TextsComponents/ParagraphComponent";
import Button from "../../components/Button/Button";
import ReactAudioPlayer from "react-audio-player";
import Music from "../../assets/win.wav";

function BoarGameWon() {
    const content = {
        title: 'Bravo !',
        content: "Super, tu as réussi à te protéger contre l'attaque du sanglier.",
        secondContent: "Lena est partie à ta recherche pour s'assurer que tu sois en sécurité et, soulagée, elle s'approche de toi. Tu comprends à ce moment-là que le monstre que tu as rencontré plus tôt ne te veut pas de mal...",
        button: "Faire sa connaissance",
        imageUrl: MainCharacter,
        link: "/level-1-choice-2",
    }

    return (
        <Content className="game-won">
            <div className="hero__background"><img src={backgroundImage} alt=""/></div>
            <div className="container center">
                <ReactAudioPlayer
                    src={Music}
                    controls={false}
                    autoPlay={true}
                />
                <ParagraphComponent title={content.title} content={content.content} image={content.imageUrl} text={content.button} link={content.link} secondContent={content.secondContent}/>
                <Button text="Retourner à l'accueil" link="/" class="thirdButton"/>
            </div>
        </Content>
    );
}

const Content = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  
  .paragraph-component {
    margin-bottom: 50px;
  }
  

  & .hero__background {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    height: 100vh;
    filter: brightness(0.6);

    img {
      object-fit: cover;
      width: 100vw;
      height: 100vh;
    }
  }
  
  & .center {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 60%;
  }
`


export default BoarGameWon;

